<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Custom_Controller {

	public function index(){
		$data = $this->configPage();
		$this->load->helper('custom_helper');

		// Load data
		$this->httprequest->init('question/recent', 'GET');
		$result = json_decode($this->httprequest->execute(), true);
		$data['recent'] = $result['payload'];

		// Load side content
		$side_data['trending_question'] = getSideContent();
		$data['side_content'] = $this->load->view('template/side_content', $side_data, TRUE);

		$this->load->view('page/index', $data);
	}

	public function do_search(){
		$data = $this->configPage();

		$this->load->view('page/index', $data);
	}
}
