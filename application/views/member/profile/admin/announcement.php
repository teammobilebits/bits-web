<?php defined('BASEPATH') OR exit('Restricted direct access') ?>

<h2>Announcement</h2> <hr>
<div class="row">
	<form class="form-horizontal" method="POST" action="admin-action.html" enctype="multipart/form-data">
		<input type="hidden" name="action" value="set">
		<div class="form-group">
			<label for="" class="control-label col-md-3">Status</label>
			<div class="col-md-8">
				
				<select class="form-control" id="toggleAnnAktif" name="toggleAnnAktif">
					<option <?php echo $announcement['isActive']=='on' ? "selected='selected'":''; ?>>on</option>
					<option <?php echo $announcement['isActive']=='off' ? "selected='selected'":''; ?>>off</option>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label for="isi_announcement" class="control-label col-md-3">Anouncement</label>
			<div class="col-md-8">
				<textarea id="txtAnn" name="txtAnn" class="form-control" rows="5" cols="10" placeholder="Please input announcement..." required><?php echo $announcement['msg']?></textarea>
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-offset-3 col-md-8">
				<input type="submit" value="Submit" class="btn btn-lg btn-primary btn-block">
			</div>
		</div>
	</form>
</div>