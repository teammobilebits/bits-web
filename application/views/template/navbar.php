<?php defined('BASEPATH') OR exit('No direct access allowed')?>

<div class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<?php echo base_url(); ?>">Logo / nama</a>
		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
			<div class="col-sm-3 col-md-6">
				<form class="navbar-form" role="search" action="<?php echo base_url(); ?>question/search" method="GET">
					<div class="input-group col-sm-12 col-md-10">
						<input type="text" class="form-control" maxlength="100" name="query" placeholder="Search...">
						<div class="input-group-btn">
							<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"> </i></button>
						</div>
					</div> 
				</form>
			</div>
			<ul class="nav navbar-nav navbar-right">
				<?php
					if($this->session->has_userdata('signed_in') && $this->session->userdata('signed_in') == true){
						$fullname = $this->session->userdata('fullname');
						echo "<li><b>Hi, $fullname</b></li>";
						//echo "<li><a href='' data-toggle='modal' data-target='#modalNewQuestion'>Ask a question</a></li>";
						echo "<li><a href='" . base_url() . "member/profilepage/my-question'>Profile</a></li>";
						echo "<li><a href='" . base_url() . "member/logout'>Logout</a><li>";
					}
					else echo "<li><a href='" . base_url() . "member/login'>Login</a></li>
								<li><a href='" . base_url() . "member/register'>Create account</a></li>";
				?>
			</ul>
		</div>
	</div>
</div>

<?php echo $navbar; ?>