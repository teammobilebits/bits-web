<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function generateTags($tagList){
	foreach($tagList as $tag){
		$url = base_url() . 'question/by-tag?tag=' . $tag['name'];
		echo " <span class='label label-primary'><a href='$url' style='text-decoration: none; color: white; font-weight: bold;'>$tag[name]</a></span>";
	}
}

function getSideContent(){
	$CI =& get_instance();
	$CI->httprequest->init('question/recent', 'GET');
	$result = json_decode($CI->httprequest->execute(), true);
	return $result['payload'];
}

function compress($source, $destination, $quality=60) {

	$info = getimagesize($source);

	if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source);
	else if ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source);
	else if ($info['mime'] == 'image/png') $image = imagecreatefrompng($source);

	imagejpeg($image, $destination, $quality);

	return $destination;
}
