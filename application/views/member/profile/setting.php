<?php defined('BASEPATH') OR exit('No direct access allowed')?>

<form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>member/updateuserinfo" enctype="multipart/form-data">
	<div class="form-group">
		<label for="inputUsername" class="control-label col-md-2">Username</label>
		<div class="col-md-7">
			<input type="text" class="form-control" name="username" id="inputUsername" value="<?php echo $username; ?>" readonly>

		</div>
	</div>

	<div class="form-group">
		<label for="inputEmail" class="control-label col-md-2">Email Address</label>
		<div class="col-md-7">
			<input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" value="<?php echo $email; ?>" required autofocus>
		</div>
	</div>

	<div class="form-group">
		<label for="inputFullName" class="control-label col-md-2">Full Name</label>
		<div class="col-md-7">
			<input type="text" id="inputFullName" name="fullname" class="form-control" placeholder="Full name" value="<?php echo $fullname; ?>" required autofocus>
		</div>
	</div>

	<div class="form-group">
		<label for="inputConfPwd" class="control-label col-md-2">Change Password</label>
		<div class="col-md-7">
			<input type="password" name="old_password" class="form-control" placeholder="Old password" autofocus>
			<input type="password" name="new_password" class="form-control" placeholder="New password" autofocus>
			<input type="password" name="conf_new_password" class="form-control" placeholder="Confirm password" autofocus>
		</div>
	</div>

	<div class="form-group">
		<label for="uploadProfilPic" class="control-label col-md-2">Change profile picture</label>
		<div class="col-md-7">
			<div class="input-group">
				<label class="input-group-btn">
					<span class="btn btn-primary">
						Browse... <input type="file" name="profpic" style="display: none;">
					</span>
				</label>
				<input type="text" class="form-control" readonly>
			</div>
		</div>
	</div>

	<div class="form-group">
		<div class="col-md-2"></div>
		<div class="col-md-7">
			<input type="submit" value="Update Information" class="btn btn-lg btn-primary btn-block">
		</div>
	</div>
</form>