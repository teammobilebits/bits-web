<?php
	require_once (APPPATH.'/libraries/Paging/Paging.php');

	class PagingCariPertanyaan extends Paging{
		private $query;

		public function __construct($params){
			$batas = $params['batas'];
			$this->query = $params['query'];
			parent::__construct($batas);
		}

		public function getLink($i){
			//echo "no halaman $i";
			return base_url() . "question/search?query=".$this->query . "&page=" . $i;
		}
	}
?>