<?php
	require_once (APPPATH.'/libraries/Paging/Paging.php');

	class PagingJawaban extends Paging{
		private $id, $seo;

		public function __construct($params){
			parent::__construct($params['batas']);
			$this->id = $params['id'];
			$this->seo = $params['seo'];
		}

		public function getLink($i){
			return base_url() . "question/".$this->id."/".$this->seo . "?page=" . $i;
		}
	}
?>