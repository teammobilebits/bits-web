<?php defined('BASEPATH') OR exit('No direct access allowed')?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php echo $head; ?>
</head>


<body>
	<div id="noJS">
		<div id="customWrap">
			<?php 
				echo $navbar;
			?>
			
			<div class="container mainContainer">
				
				<div class="col-md-12">
					<h2><?php echo $question['title']; ?></h2> <hr>
				</div>
				<div class="col-sm-8 col-md-8">
					<div class="row">
						<div class="col-md-2">
							<div class="well well-sm">
								<img src="<?php echo $question['userpic']; ?>" class="img-responsive img-rounded">
								<small><b><center><?php echo $question['askedby']; ?></center></b></small> <br>
								<p style="font-weight: 50px;" style="margin-bottom: 0px; font-size: 100px;"><small>Asked on <?php echo $question['date'] ?></small></p>
							</div>
						</div>
						<div class="col-md-10">
							<p><?php echo htmlspecialchars_decode($question['content']); ?></p>
							<p>
								<?php generateTags($question['tags']); ?>
							</p>
						</div>
					</div>

					<div style="margin-top: 50px;">
						<?php 

							echo "<h3>".$answer['len']." answers</h3> <hr>";
							$i = 0;
							foreach($answer['answerList'] as $ans){ ?>
								<div class="row">
									<div class="col-md-2">
										<div class="well well-sm">
											<img src="http://developer.rudii14.xyz/v2/user_img/<?php echo $ans['userpic']; ?>" class="img-responsive img-rounded">
											<small><b><center><?php echo $ans['answeredby']; ?></center></b></small> <br>
											<p style="font-weight: 50px;" style="margin-bottom: 0px; font-size: 100px;"><small>Asked on <?php echo tgl_indo($ans['date'], TGL_PENDEK); ?></small></p>
										</div>
										<div class="col-md-12" style="margin-bottom: 20px;">
											<?php
												if($answer['answered'] == true && $i==0 && $posisi==0){
													echo "<center><span class='glyphicon glyphicon-ok' style='color:green; font-size:30px;'></span></center>";
													$i++;
												}
											?>
										</div>
										<!--<div class="col-md-12">
											<?php
												/*if(checkUserSession($_SESSION) && $_SESSION['username'] ==  $questionDetails['askedby']){
													echo "<div class='btn btn-success' onclick='setAnswer($id, ".$ans['id_answer'].")'><span class='glyphicon glyphicon-ok'></span> Tick</div>";
												}*/
											?>
										</div>-->
									</div>
									<div class="col-md-10">
										<p><?php echo htmlspecialchars_decode($ans['answer']); ?></p>
									</div>
								</div> <hr>
						<?php } ?>

						<?php
							echo "<div class='row'><div class='pull-right'>".$paging->generateNavHalaman($page, $answer['len'])."</div></div>";
						?>
					</div>

					<div style="margin-top: 30px;">
						<?php if($this->session->has_userdata('signed_in') && $this->session->userdata('signed_in')==true){ ?>
							<h3>Your Answer</h3> <hr>
							<div class="elementHide" id='qid'><?php echo $question['id']; ?></div>
							<div class="elementHide" id='username'><?php echo $_SESSION['username']; ?></div>
							<div class="elementHide" id='seo'><?php echo $seo; ?></div>
							<div id="answereditor-container" style="height: 250px;"></div>
							<div class="col-md-12" style="padding-top: 20px;"><div class="btn btn-primary pull-right" id="btnSubmitAnswer">Submit</div></div>
						<?php } else { ?>
							<p>Please <a href="<?php echo base_url(); ?>member/login">sign in</a> to submit your answer to this question</p>
						<?php } ?>
					</div>
				</div>

				<?php echo $side_content; ?>
			</div>
		</div>
	</div>
	

	<?php echo $footer;?>
	<?php echo $script; ?>
</body>
</html>