/*
	GLOBAL VARIABLE SETTING
*/
//var baseLink = 'http://developer.rudii14.xyz/v2/';
var baseLink = 'http://localhost/bitsAPI_2/';
var webBaseLink = 'http://localhost/bitsForum/';
var debug = true;

/*if(debug){
	baseLink = "http://localhost/tugas_pemweb/api/";
	webBaseLink = "http://localhost/tugas_pemweb/web/"
}
else{
	baseLink = "http://developer.rudii14.xyz/";
	webBaseLink = "http://rudi.xyz/"
}*/


/*
	DEFINE FUNCTION
*/
function escapeHtml(text) {
	var map = {
		'&': '&amp;',
		'<': '&lt;',
		'>': '&gt;',
		'"': '&quot;',
		"'": '&#039;'
	};
	return text.replace(/[&<>"']/g, function(m) { return map[m]; });
}

function htmlDecode(val){
	var textArea = document.createElement('textarea');
	textArea.innerHTML = val
	return textArea.value
}

function setAnswer(qID, aID){
	console.log(aID + ' ' + qID)
	$.ajax({
		type: "GET",
		url: baseLink + "answer",
		data: {mode:'tick', qid:qID, aid:aID},
		success: function(data){
			location.href = location.href
		}
	})
}

$(document).ready(function(){
	if(!$('#toggleAnnouncement').is(":checked")) $('#formAnnouncement').hide();
	$('#toggleAnnouncement').click(function(){
		var divForm =  $('#formAnnouncement');
		if($('#toggleAnnouncement').is(":checked")){
			divForm.slideDown();
			$('#isActive').val('on');
		}
		else{
			divForm.slideUp();
			$('#isActive').val('off');
		}
		

		$.ajax({
			type: "GET",
			url: baseLink + "announcement",
			data: "action=set&status=" + $('#isActive').val() + "&content=" + $("#txtAnn").val()
		});
	});

	// datatable
	$("#tblUserList").DataTable({
		"columnDefs": [
			{ "orderable": false, "targets": 5 }
		]
	});

	$("#tblReportList").DataTable({
		"columnDefs": [
			{ "orderable": false, "targets": 6 }
		]
	});

	$("#tblQuestionList").DataTable({
		"columnDefs": [
			{ "orderable": false, "targets": [2,3] }
		]
	});


	$("#tblAnswerList").DataTable({
		"columnDefs": [
			{ "orderable": false, "targets": [2,3] }
		]
	});

	$("#tblAdvertisement").DataTable({
		"columnDefs": [
			{ "orderable": false, "targets": 4 }
		]
	});
	

	if($('#toggleAnnAktif :selected').text() == 'off') $('#txtAnn').prop('readonly', true)
	$('#toggleAnnAktif').click(function(){
		var selected = $('#toggleAnnAktif :selected').text();

		$('#txtAnn').val('');
		if(selected == 'off') $('#txtAnn').prop('readonly', true);
		else $('#txtAnn').prop('readonly', false);
	})


	/*
		Semua bagian modal di page admin
	*/
	// Elemeent yang mau di hide/show
	var loading = $('#loading');
	var searchForm = $('#seachBox');
	var searchResult = $('#searchResult');
	var errorDialog = $('#errorDialog');

	$('#searchBtn').click(function(){
		var username = $('#userAdmin').val();

		searchForm.hide();
		loading.fadeIn();

		$.ajax({
			type: "GET",
			url: baseLink + "user",
			data: {mode:'getuser', username:username},
			dataType: "json",
			success: function(data){
				loading.hide();

				if(data.success == false){
					$('#errorMsg').html(data.msg)
					errorDialog.fadeIn(500);
				}
				else{
					$('#userPic').attr("src", "userpic/"+data.userdata.foto)
					$('#dispUsername').html(username)
					$('#btnAddAsAdmin').val(username)
					searchResult.fadeIn(1000);
				}
			}
		})
	})

	$('#modalClose').click(function(){
		loading.hide();
		searchResult.hide();
		errorDialog.hide();

		searchForm.show();
		$('#userAdmin').val('')
	})

	$('#btnAddAsAdmin').click(function(){
		var username = $('#btnAddAsAdmin').val();

		searchResult.hide();
		loading.fadeIn();

		$.ajax({
			type: "GET",
			url: baseLink + "user",
			data: "mode=changetype&username=" + username + "&type=1",
			dataType: "json",
			success: function(data){
				loading.hide();

				if(data.success == false){
					$('#errorMsg').html(data.msg)
					errorDialog.fadeIn(500);
				}
				else{
					location.reload();
				}
			}
		})
	})
})

/*
	QUILL EDITOR INITIALISATION
*/
var questionEditor= new Quill('#questioneditor-container', {
	modules: {
		toolbar: [
			[{ header:[1,2,3,4,5,6,false] }],
			['bold', 'italic', 'underline', 'strike'],
			['code-block', 'blockquote'],
			[{'list':'ordered'}, {'list':'bullet'}],

			[{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
			[{ 'font': [] }],
			[{ 'align': [] }],

			['clean']
		]
	},
	theme: 'snow'
})

var answerEditor= new Quill('#answereditor-container', {
	modules: {
		toolbar: [
			[{ header:[1,2,3,4,5,6,false] }],
			['bold', 'italic', 'underline', 'strike'],
			['code-block', 'blockquote'],
			[{'list':'ordered'}, {'list':'bullet'}],

			[{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
			[{ 'font': [] }],
			[{ 'align': [] }],

			['clean']
		]
	},
	theme: 'snow'
})


$('#btnResetQuill').click(function(){
	var questionID = $('#qID').html();

	if(questionID == null) questionEditor.setText('');
	else $('#modalNewQuestion').modal('hide')
})

$('#btnSubmitEditor').click(function(){
	var questionID = $('#qID').html();

	var content = questionEditor.root.innerHTML;
	var tags = $('#txtTags').val();
	var username = $('#qusername').val();
	var question = $('#txtQuestion').val();

	// Hide and show
	$('#containerQuestion').hide()
	$('#containerLoading').fadeIn()


	if(questionID == ''){
		console.log(content + " " + tags + ' ' + username + " " + question);

		/*$.post(baseLink + "question/add",{
			username:username,
			tags:tags,
			details:escapeHtml(content),
			question:question
		},
		function(data, status){
			alert("Data: " + data + "\nStatus: " + status);
		});*/

		$.ajax({
			type: "POST",
			dataType: "json",
			url: baseLink + "question/add",
			data: {
				username:username,
				tags:tags,
				details:escapeHtml(content),
				question:question
			},
			success: function(data){
				console.log(data)
				data = data.payload;
				$url = webBaseLink + "question/" + data.id + "/" + data.seo;
				location.href = $url;
			}
		})
	}
	else{
		$.ajax({
			type: "POST",
			dataType: "json",
			url: baseLink + "question/update",
			data: {
				id:questionID,
				tags:tags,
				details:escapeHtml(content),
				question:question
			},
			success: function(){
				location.href = location.href
			}
		})
	}
})

$('#btnSubmitAnswer').click(function(){
	var _content = answerEditor.root.innerHTML;
	var _username = $('#username').html()
	var _qid = $('#qid').html()

	var cData = {qid: _qid, username: _username, answer: escapeHtml(_content)}
	console.log(cData);
	console.log("asd");

	$.ajax({
		type: "POST",
		url: baseLink + "answer/add",
		dataType: "json",
		data: {
			qid: _qid,
			username: _username,
			answer: escapeHtml(_content)
		},
		success: function(data){
			console.log(data);
			location.href = location.href
		}
	})
})
/*
$('.btn-set-question').click(function(){
	var aID = $('#aID').html();
	var qID = $('#qID').html();

	console.log(aID + ' ' + qID)
	$.ajax({
		type: "GET",
		url: baseLink + "answer",
		data: {mode:'tick', qid:qID, aid:aID},
		success: function(data){
			location.href = location.href
		}
	})
})*/


/*
	INPUT FILE TYPE PRETIFY
*/
$(function() {
	// We can attach the `fileselect` event to all file inputs on the page
	$(document).on('change', ':file', function() {
		var input = $(this),
			numFiles = input.get(0).files ? input.get(0).files.length : 1,
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		
		input.trigger('fileselect', [numFiles, label]);
	});

	// We can watch for our custom `fileselect` event like this
	$(document).ready( function() {
		$(':file').on('fileselect', function(event, numFiles, label) {

			var input = $(this).parents('.input-group').find(':text'),
				log = numFiles > 1 ? numFiles + ' files selected' : label;

			if( input.length ) input.val(log); 
			else if( log ) alert(log);
		});
	});
});




/*

	MODAL JAVASCRIPT

*/
$('#deleteConf').on('show.bs.modal', function(e) {
	var questionID = $(e.relatedTarget).data('id');
	$('#btnYes').click(function(){
		$.ajax({
			type: "POST",
			url: baseLink + "question/delete",
			data: {id:questionID},
			dataType: "json",
			success: function(data){
				location.href = location.href;
			}
		})
	})
});

$('#modalEditReport').on('show.bs.modal', function(e) {
	var issuer = $(e.relatedTarget).data('issuer');
	var type = $(e.relatedTarget).data('type');
	var details = $(e.relatedTarget).data('details');
	var date = $(e.relatedTarget).data('date');
	var status = $(e.relatedTarget).data('status');
	var action = $(e.relatedTarget).data('action');

	console.log(status)

	$('#txtIssuer').html(issuer);
	$('#txtType').html(type);
	$('#txtDetails').html(details);
	$('#txtDate').html(date);
	$('#txtAction').html(action);
	if(status == 'N'){
		$('#lblStatus').addClass("lbl-success").html('SOLVED');
		$('#rowAction').show();
		$('#txtAction').html(action);
	}
	else{
		$('#lblStatus').addClass('lbl-danger').html('UNSOLVED');
	}
});

$('#modalAdminEditUser').on('show.bs.modal', function(e) {
	// Ambil data yang dipassing
	var username = $(e.relatedTarget).data('username');
	var email = $(e.relatedTarget).data('email');
	var usertype = $(e.relatedTarget).data('usertype');
	var blokir = $(e.relatedTarget).data('blokir');
	var blokir_alasan = $(e.relatedTarget).data('alasanblokir');
	var userpic = $(e.relatedTarget).data('userpic');

	// Input id dari form
	var editUsername = $('#editUsername');
	var editEmail = $('#editEmail');
	var editType = $('#editType');
	var editBlokir = $('#toggleBlokir');
	var editAlasanBlokir = $('#blokir_alasan');
	var editUserpic = $('#userpic');

	// Set value form
	$('#username').val(username);
	editUsername.html(username);
	editEmail.html(email);
	editType.val(usertype=='admin' ? 'Administrator' : 'User');
	editBlokir.val(blokir=='Y' ? 'Ya' : 'Tidak')
	if(blokir == 'Y'){
		editAlasanBlokir.val(blokir_alasan);
	}
	editUserpic.prop('src', 'userpic/'+userpic);

	if($('#toggleBlokir :selected').text() == 'Tidak') $('#blokir_alasan').prop('readonly', true)
	editBlokir.click(function(){
		var selected = $('#toggleBlokir :selected').text();

		editAlasanBlokir.val('');
		if(selected == 'Tidak') editAlasanBlokir.prop('readonly', true);
		else editAlasanBlokir.prop('readonly', false);
	})	
});

$('#modalNewQuestion').on('show.bs.modal', function(e) {
	var questionID = $(e.relatedTarget).data('id');
	console.log(questionID)
	$('#qID').html(questionID)
	var seo = $(e.relatedTarget).data('seo');

	var txtQuestion = $(this).find('#txtQuestion')
	var txtTags = $(this).find('#txtTags')
	if(questionID != null){
		$(this).find('.modal-title').html('Edit your question')
		$(this).find('#btnResetQuill').html('Cancel')
		

		// Ambil data pertanyaan based on the id
		$.ajax({
			type: "GET",
			url: baseLink + "question/id/" + questionID + "/false",
			dataType: "json",
			success: function(data){
				// Isi semua formnya
				data = data.payload;
				txtQuestion.val(data.title)
				var tagList = ''
				var tags = data.tags;
				console.log(tags)
				for(var i in tags){
					if (tags.hasOwnProperty(i)) {
						if(tagList != '') tagList += ",";
						tagList += tags[i].name
					}
				}
				txtTags.val(tagList)
				questionEditor.root.innerHTML = htmlDecode(data.content)
			}
		})
	}
	else{
		questionEditor.root.innerHTML = '';
		txtQuestion.val('');
		txtTags.val('')
	}
});