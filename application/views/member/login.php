<!DOCTYPE html>
<html lang="en">
<head>
	<?php echo $head; ?>
</head>


<body>
	<div id="noJS">
		<div id="customWrap">
			<?php echo $navbar; ?>
			
			<div class="container mainContainer">
				<div class="col-sm-12 col-md-12 formContainer">
					<div class="col-sm-12 col-md-offset-4 col-md-4">
						<?php if($msg != null){ ?>
						<div class="alert alert-dismissable fade in" style="background-color: red;">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
								<?php echo $msg; ?>
						</div>
						<?php } ?>
						
						<form method="POST" action="<?php echo base_url() . 'member/do-login'; ?>">
							<h2>UMN Stackoverflow login</h2>
							<div class="form-group">
								<label for="inputEmail" class="sr-only">Username</label>
								<input name="username" type="text" id="inputEmail" class="form-control" placeholder="Username" required autofocus>
							</div>

							<div class="form-group">
								<label for="inputPwd" class="sr-only">Password</label>
								<input name="password" type="password" id="inputPwd" class="form-control" placeholder="Password" required autofocus>
							</div>

							<div class="form-group">
								<input type="submit" value="Sign in" class="btn btn-lg btn-primary btn-block">
							</div>
						</form>

						<p>Tidak punya akun? Silahkan <a href="<?php echo base_url() . 'member/register'; ?>">daftar</a> sekarang</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	

	<?php echo $footer;?>
	<?php echo $script; ?>
</body>
</html>