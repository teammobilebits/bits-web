<?php
	
	class Paging{
		protected $batas = 0;

		public function __construct($batas){
			$this->batas = $batas;
		}

		public function cariPosisi(){
			if(empty($_GET['page'])){
				$posisi = 0;
				$_GET['page'] = 1;
			}
			else $posisi = ($_GET['page']-1) * $this->batas;

			return $posisi;
		}

		public function getBatas(){
			return $this->batas;
		}

		protected function jumlahHalaman($jmlData){
			return ceil($jmlData/$this->batas);
		}

		public function generateNavHalaman($halamanAktif, $jmlData){
			$link_halaman = "";
			$jmlhalaman = $this->jumlahHalaman($jmlData);
			//echo $jmlhalaman . ' ' . $jmlData;

			// Link ke halaman pertama (first) dan sebelumnya (prev)
			$link_halaman .= "<ul class='pagination'>";
			if($halamanAktif > 1){
				$prev = $halamanAktif-1;
				$link_halaman .= "<li><a href='".$this->getLink(1)."'><< First</a></li>  <li><a href='".$this->getLink($halamanAktif-1)."'>< Prev</a></li>";
			}
			else{ 
				$link_halaman .= "<li class='disabled'><a><< First</a></li> <li class='disabled'><a>< Prev</a></li>";
			}

			// Link halaman 1,2,3, ...
			$angka = ($halamanAktif > 3 ? "<li class='disabled'><a href=''>...</a></li>" : " "); 
			for ($i=$halamanAktif-2; $i<$halamanAktif; $i++){
				if ($i < 1) continue;
				$angka .= "<li><a href='".$this->getLink($i)."'>$i</a></li>";
			}
			$angka .= "<li class='active'><a href='".$this->getLink($halamanAktif)."'>$halamanAktif</a></li>";
	  
			for($i=$halamanAktif+1; $i<($halamanAktif+3); $i++){
				if($i > $jmlhalaman) break;
				$link = $this->getLink($i);
				$angka .= "<li><a href='$link'>$i</a></li>";
			}
	  		$angka .= ($halamanAktif+2<$jmlhalaman ? "<li class='disabled'><a href=''>...</a></li><li><a href='".$this->getLink($jmlhalaman)."'>$jmlhalaman</a></li>" : " ");

			$link_halaman .= "$angka";

			// Link ke halaman berikutnya (Next) dan terakhir (Last) 
			if($halamanAktif < $jmlhalaman){
				$next = $halamanAktif+1;
				$link_halaman .= " <li><a href='".$this->getLink($halamanAktif+1)."'>Next ></a></li>  <li><a href='".$this->getLink($jmlhalaman)."'>Last >></a></li>";
			}
			else $link_halaman .= "<li class='disabled'><a>Next ></a></li> <li class='disabled'><a>Last >></a></li>";
			
			$link_halaman .= "</ul>";
			return $link_halaman;
		}

		// ni function di overide di tiap turunan
		public function getLink($i){}
	}
?>