<?php defined('BASEPATH') OR exit('No direct access allowed')?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php echo $head; ?>
</head>


<body>
	<div id="noJS">
		<div id="customWrap">
			<?php echo $navbar; ?>
			
			<div class="container mainContainer">
				<div class="col-sm-12 col-md-12">
					<?php echo $msg; ?>
				</div>
				
			</div>
		</div>
	</div>
	

	<?php echo $footer;?>
	<?php echo $script; ?>
</body>
</html>