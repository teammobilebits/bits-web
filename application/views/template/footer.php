<?php defined('BASEPATH') OR exit('No direct access allowed')?>

<div class="footer">
	<div class="container">
		<p class="text-muted credit text-center" style="margin-top: 10px;">
			UMN STACKOVERFLOW © 2017 <a href="http://www.linkedin.com/in/rudiyanto1410" target="_blank"><b>Rudiyanto</b></a> & Ricky Cuandi. All Rights Reserved
		</p>
	</div>
</div>