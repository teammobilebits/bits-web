<?php defined('BASEPATH') OR exit('No direct access allowed')?>

<h2>Report List</h2> <hr>
<!--<div class="row" style="margin-bottom:10px; margin-right: 1px;">
	<a class="btn btn-primary pull-right" data-toggle="modal" data-target="#modalAddAdmin"><i class="glyphicon glyphicon-plus-sign"></i> Admin</a>
</div> -->
<div class="row" style="margin-bottom: 20px;">
	<div class="col-md-6">
		<form class="form-inline" method="GET" action="<?php echo base_url(); ?>member/profilepage/report-list">
			<div class="form-group">
				<label for="filter">Filter by status:</label>
				<select class="form-control" name="filter">
					<option value="all" <?php echo $filter=='all' ? "selected='selected'" : "";?>>All</option>
					<option value="solved" <?php echo $filter=='solved' ? "selected='selected'" : "";?>>Solved</option>
					<option value="unsolved" <?php echo $filter=='unsolved' ? "selected='selected'" : "";?>>Unsolved</option>
				</select>
			</div>
			<button type="submit" class="btn btn-default">Filter</button>
		</form>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<table id="tblReportList" class="table table-striped table-bordered" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>#</th>
					<th>Issuer</th>
					<th>Report Type</th>
					<th>Details</th>
					<th>Issued Date</th>
					<th>Status</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php

					$i = 1;
					foreach($reportList as $r){
						$type = '';
						$details = '';
						$obj = json_decode($r['details'], true);
						switch ($r['type']) {
							case 'USER':{
								$type = 'User Account Report';
								$details = '<p><b>Reported User</b>: ' . $obj['username'] . ' ( ID: '.$obj['userid'].' )</p> <p><b>Details</b>: '.$obj['details'].'</p>';
								break;
							}
							case 'QUESTION':{
								$type = 'Question Report';
								$link = base_url() . "question/" . $obj['id'] . "/" . $obj['seo'];
								$details = "<p><b>Reported Question</b>: <a href='$link'>$obj[title]</a></p> <p><b>Details</b>: $obj[details]</p>";
								break;
							}
							case 'ANSWER':{
								$type = 'Answer Report';
								$link = base_url() . "question/" . $obj['id'] . "/" . $obj['seo'];
								$details = "<p><b>Reported Question</b>: <a href='$link'>$obj[title]</a></p> <p><b>Details</b>: $obj[details]</p>";
								break;
							}
						}
						$indoFormat = tgl_indo($r['date'], $flag=TGL_PANJANG);
						$action = null;
						if($r['action_by'] != null){
							$action = "<p><b>Action</b>: $r[action]<br /><b>Action taken by</b>: $r[action_by]<br /><b>Date</b>: ".tgl_indo($r['action_date'])."</p>";
						}
						echo "<tr>
								<td>$i</td>
								<td>".$r['username']."</td>
								<td>".$type."</td>
								<td>".$details."</td>
								<td>".$indoFormat."</td>
								<td>".($r['active']=='Y' ? "<span class='label label-danger'>UNSOLVED</span>" : "<span class='label label-success'>SOLVED</span>")."</b></td>
								<td>
									<a
										data-toggle='modal'
										data-issuer = '$r[username]'
										data-type = '$type'
										data-details = ''
										data-date = '$indoFormat'
										data-status = '$r[active]'
										data-action = '$action'
										id='editReportModal'
										data-target='#modalEditReport' class='btn btn-primary btn-sm'><i class='glyphicon glyphicon-wrench remove'></i> Edit</a>
								</td>
							</tr>";

							/**/
						$i++;
					}
				?>
			</tbody>
		</table>
	</div>
</div>

<!-- Modal untuk nambah admin -->
<div class="modal fade" id="modalEditReport" role="dialog">
	<div class="vertical-alignment-helper">
		<div class="modal-dialog vertical-align-center">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" id="modalClose"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h3 class="modal-title">Edit user</h3>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-2">
							<b>Issuer</b>
						</div>
						<div class="col-md-6">
							<p id="txtIssuer"></p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2">
							Report Type
						</div>
						<div class="col-md-6">
							<p id="txtType"></p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2">
							Details
						</div>
						<div class="col-md-6">
							<p id="txtDetails"></p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2">
							Issued date
						</div>
						<div class="col-md-6">
							<p id="txtDate"></p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2">
							Status
						</div>
						<div class="col-md-6">
							<span class='label' id="lblStatus"></span>
						</div>
					</div>
					<div class="row" id="rowAction" style="display: none;">
						<div class="col-md-2">Action</div>
						<div class="col-md-6" id="txtAction">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>