<?php defined('BASEPATH') OR exit('No direct access allowed')?>

<div class="modal fade" id="modalNewQuestion" role="dialog">
	<div class="vertical-alignment-helper">
		<div class="modal-dialog modal-lg vertical-align-center">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" id="modalNewQuestionClose"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h3 class="modal-title">Ask a Question!</h3>
				</div>
				<div class="modal-body">
					<div id="containerQuestion">
						<div class="row" style="padding-bottom: 20px;">
							<div class="col-md-12"><input type="text" id="txtQuestion" class="form-control" placeholder="Question"></div>
						</div>
						<div id="questioneditor-container" style="height: 250px;"></div>

						<div class="row" style="padding-top: 20px;">
							<input type="hidden" id="qusername" value="<?php echo $_SESSION['username'];?>">
							<div class="col-md-6">
								<input type="text" id="txtTags" class="form-control" placeholder="Question tags seperated with , (coma)">
							</div>
							<div class="col-md-3">
								<button class="btn btn-primary btn-block" id="btnSubmitEditor">Submit</button>
							</div>
							<div class="col-md-3">
								<button class="btn btn-danger btn-block" id="btnResetQuill">Reset</button>
							</div>
						</div>
					</div>
					<img src="<?php echo base_url(); ?>/assets/img/loading.gif" id="containerLoading" style="display:none;">
				</div>
				<div id="qID" style="display: none;"></div>
			</div>
		</div>
	</div>
</div>