<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Question extends CI_Custom_Controller {

	public function index($id=null, $seo=null){
		// config and load necessary lib & helper
		$this->load->helper('format_date');
		$this->load->helper('custom_helper');

		if(is_null($id) || is_null($seo)){
			$data = $this->configPage();
			$this->load->view('errors/custom/error_404', $data); 
			return;
		}
		$plusView = ($this->input->get('page') != null);
		// Load data question
		$this->httprequest->init("question/id/$id/".$plusView, 'GET');
		$result = json_decode($this->httprequest->execute(), true);
		$result['payload']['userpic'] = "http://developer.rudii14.xyz/v2/user_img/" + $result['payload']['userpic'];
		$result['payload']['date'] = tgl_indo($result['payload']['date'], TGL_PENDEK);
		$this->session->set_userdata('title', $result['payload']['title']);

		$data = $this->configPage();
		$data['question'] = $result['payload'];

		// Config paging untuk jawaban
		$batas = 5;
		$params = array(
					"batas" => $batas,
					"id"    => $id,
					"seo"   => $seo
				);
		$this->load->library('Paging/PagingJawaban', $params);
		$p = $this->pagingjawaban;
		$data['posisi'] = $p->cariPosisi();
		$data['paging'] = $p;

		// Load data jawaban
		$this->httprequest->init("answer/fromquestion", 'POST');
		$this->httprequest->setParameter('qid', $id);
		$this->httprequest->setParameter('start', $data['posisi']);
		$this->httprequest->setParameter('batas', $batas);
		$result = json_decode($this->httprequest->execute(), true);
		//var_dump($result);
		$data['answer'] = $result['payload'];
		$data['page'] = $this->input->get('page');

		// Load data related question
		$this->httprequest->init("question/trending", 'GET');
		$result = json_decode($this->httprequest->execute(), true);
		$side_data['trending_question'] = $result['payload'];

		// Load data related question
		$this->httprequest->init("question/related", 'GET');
		$this->httprequest->setParameter('id', $id);
		$result = json_decode($this->httprequest->execute(), true);
		$side_data['related'] = $result['payload'];

		$data['side_content'] = $this->load->view('template/side_content', $side_data, TRUE);

		$this->load->view('question/index', $data);
	}


	public function search(){
		$data = $this->configPage();
		$this->load->helper('custom_helper');

		$query = $this->input->get('query');
		$data['query'] = $query;

		// Load side content
		$side_data['trending_question'] = getSideContent();
		$data['side_content'] = $this->load->view('template/side_content', $side_data, TRUE);

		$batas = 3;
		$params = array(
					"batas" => $batas,
					"query"    => $query
				);
		$this->load->library('Paging/PagingCariPertanyaan', $params);
		$p = $this->pagingcaripertanyaan;
		$data['posisi'] = $p->cariPosisi();
		$data['paging'] = $p;
		$data['page'] = $this->input->get('page');

		$this->httprequest->init('question/search', 'GET');
		$this->httprequest->setParameter('query', $query);
		$this->httprequest->setParameter('start', $data['posisi']);
		$this->httprequest->setParameter('batas', $batas);
		$result = json_decode($this->httprequest->execute(), true);
		$data['result'] = $result['payload'];

		$this->load->view('question/search', $data);
	}

	public function by_tag(){
		$this->load->helper('custom_helper');
		$data = $this->configPage();

		$tagname = $this->input->get('tag');
		$data['tagname'] = $tagname;

		// Load side content
		$side_data['trending_question'] = getSideContent();
		$data['side_content'] = $this->load->view('template/side_content', $side_data, TRUE);

		$batas = 5;
		$params = array(
					"batas" => $batas,
					"tagname" => $tagname
				);
		$this->load->library('Paging/PagingPertanyaanByTag', $params);
		$p = $this->pagingpertanyaanbytag;
		$data['posisi'] = $p->cariPosisi();
		$data['paging'] = $p;
		$data['page'] = $this->input->get('page');

		$this->httprequest->init('question/by-tag', 'GET');
		$this->httprequest->setParameter('tagname', $tagname);
		$this->httprequest->setParameter('start', $data['posisi']);
		$this->httprequest->setParameter('batas', $batas);
		$result = json_decode($this->httprequest->execute(), true);
		if($result['success'] == false) echo $result['errMsg'];
		$data['result'] = $result['payload'];

		$this->load->view('question/tag', $data);
	}
}
