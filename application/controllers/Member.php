<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Custom_Controller {

	
	public function index(){
		redirect(base_url() . "/member/profil");
	}

	/*
		Bagian login
	*/
	public function login(){
		$data = $this->configPage();

		$data['msg'] = $this->session->flashdata('msg_login');
		$this->load->view('member/login', $data);
	}
	public function do_login(){
		$data = $this->configPage();

		$username = $this->input->post('username');
		$password = $this->input->post('password');

		if(is_null($username) || is_null($password)){
			$this->load->view('member/login', $data);
		}

		// login ke server
		$this->httprequest->init('user/login', 'POST');
		$this->httprequest->setParameter('username', $username);
		$this->httprequest->setParameter('password', $password);
		$result = json_decode($this->httprequest->execute(), true);

		if($result['success'] == false){
			if($result['errMsg'] == 'WRONG_USERNAME_PASSWORD_COMBINATION' || $result['errMsg'] == 'INVALID_ARGUMENTS'){
				$this->session->set_flashdata('msg_login', 'Wrong username and password combination');
			}
			else if($result['errMsg'] == 'ACCOUNT_SUSPENDED'){
				$this->session->set_flashdata('msg_login', 'Your account has been suspended');
			}
			redirect(base_url() . 'member/login');
		}
		else{
			$payload = $result['payload'];
			$this->session->set_userdata('signed_in', true);
			$this->session->set_userdata('username', $payload['username']);
			$this->session->set_userdata('userType', $payload['type']);
			$this->session->set_userdata('fullname', $payload['fullname']);
			$this->session->set_userdata('email', $payload['email']);
			$this->session->set_userdata('userpic', "http://developer.rudii14.xyz/v2/user_img/" . $payload['img']);
			redirect(base_url());
		}
	}

	public function register($module=''){
		$data = $this->configPage();

		if($module == ''){
			$data['msg'] = $this->session->flashdata('msg_register');
			$this->load->view('member/register', $data);
		}
		else if($module == 'success'){
			$data['msg'] = $this->session->userdata('successMsg');
			$this->load->view('template/blank', $data);
		}
	}
	public function do_register(){
		// Do some shit here 
		$input = $this->input;

		$this->directAccessControl($input->post('email')!=null);

		$email = $input->post('email');
		$username = $input->post('username');
		$password = $input->post('password');
		$fullname = $input->post('fullname');
		$confpassword = $input->post('confpassword');

		$this->httprequest->init('user/register', 'POST');
		$this->httprequest->setParameter('username', $username);
		$this->httprequest->setParameter('password', $password);
		$this->httprequest->setParameter('email', $email);
		$this->httprequest->setParameter('confPass', $confpassword);
		$this->httprequest->setParameter('fullname', $fullname);
		$result = json_decode($this->httprequest->execute(), true);

		if($result['success']){
			// create user success
			$loginUrl = base_url() . "member/login";
			$successMsg =  "
				<div class='col-sm-8 col-md-8'>
					<p>Akun anda sudah dibuat dan sudah bisa digunakan. Silahkan <a href='$loginUrl'>login</a></p>
				</div>
			";
			$this->session->set_flashdata('successMsg', $successMsg);
			redirect(base_url() . 'member/register/success');
		}
		else{ 
				$msg = '';
				switch ($result['errMsg']) {
					case 'INVALID_ARGUMENTS':{
						$msg = 'All fields is required';
						break;
					}
					case 'EMAIL_EXISTS':{
						$msg = 'That E-mail has been used by another user';
						break;
					}
					case 'USERNAME_EXISTS':{
						$msg = 'That Username has been used';
						break;
					}
					case 'PASSWORD_NOT_MATCH':{
						$msg = 'Your password is not match';
						break;
					}
				}

				$this->session->set_flashdata('msg_register', $msg);
				redirect(base_url() . "member/register");
		}
	}

	public function profilepage($module){
		if(!$this->session->has_userdata('signed_in') || !$this->session->userdata('signed_in')){
			redirect(base_url() . 'member/login');
		}

		// Load template
		$data = $this->configPage();
		$sidebarData = array(
			'userpic' => $this->session->userdata('userpic'),
			'fullname' => $this->session->userdata('fullname'),
			'userType' => $this->session->userdata('userType')
		);
		
		$content = array();

		if($module == "my-question"){
			$sidebarData['sidemenu'] = 'overview';

			$this->httprequest->init('question/userquestion', 'GET');
			$this->httprequest->setParameter('username', $this->session->userdata('username'));
			$result = json_decode($this->httprequest->execute(), true);
			$content['userQuestion'] = $result['payload'];

			$data['mainContent'] = $this->load->view('member/profile/question', $content, TRUE);
		}
		else if($module == "setting"){
			$sidebarData['sidemenu'] = '';

			$content = array(
				'username' => $this->session->userdata('username'),
				'email' => $this->session->userdata('email'),
				'fullname' => $this->session->userdata('fullname')
			);

			$data['mainContent'] = $this->load->view('member/profile/setting', $content, TRUE);
		}

		// Admin section
		else if($module == "report-list"){
			$this->load->helper('format_date_helper');
			$sidebarData['sidemenu'] = 'admin';

			$filter = $this->input->get('filter');
			if(is_null($filter)) $filter = 'unsolved';

			$this->httprequest->init('report/get/'.$filter, 'GET');
			$result = json_decode($this->httprequest->execute(), true);
			$content['reportList'] = $result['payload'];
			$content['filter'] = $filter;

			$data['mainContent'] = $this->load->view('member/profile/admin/reportlist', $content, TRUE);
		}
		else if($module == "announcement"){
			$sidebarData['sidemenu'] = 'admin';


		}
		else if($module == "user-list"){
			$sidebarData['sidemenu'] = 'admin';

			$this->httprequest->init('user/userlist', 'GET');
			$result = json_decode($this->httprequest->execute(), true);
			$content['userlist'] = $result['payload'];

			$data['mainContent'] = $this->load->view('member/profile/admin/userlist', $content, TRUE);
		}
		else{
			// page not found
		}
		$data['sidebar'] = $this->load->view('template/member_sidebar', $sidebarData, TRUE);

		$this->load->view('member/profile/index', $data);
	}

	public function updateuserinfo(){
		$this->load->helper('custom_helper');

		$username = $this->input->post('username');
		$email = $this->input->post('email');
		$fullname = $this->input->post('fullname');
		$old_password = $this->input->post('old_password');
		$new_password = $this->input->post('new_password');
		$conf_password = $this->input->post('conf_new_password');

		//init request
		$this->httprequest->init("user/update/$username", 'POST');
		$this->httprequest->setParameter('email', $email);
		$this->httprequest->setParameter('full_name', $fullname);

		// Cek perubahan di password
		if($old_password!='' && $new_password!='' && $conf_password!=''){
			$this->httprequest->setParameter('oldpassword', $old_password);
			$this->httprequest->setParameter('confpassword', $conf_password);
			$this->httprequest->setParameter('newpassword', $new_password);
		}

		// Config untuk upload
		$config['upload_path'] = './uploads';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = '5000';
		$config['file_name'] = $username;
		$this->load->library('upload', $config);

		$success = $this->upload->do_upload('profpic');

		if($success){
			$upload_data = $this->upload->data();
			$profpic = $upload_data['file_name'];
			$ratio = 60;
			if($upload_data['file_size'] > 2000) $ratio = 10;
			else if($upload_data['file_size'] > 1000) $ratio = 20;
			else $ratio = 60;

			compress($upload_data['full_path'], $upload_data['full_path'], $ratio);

			// Encode to string
			$base64 = 'data:image/' . $upload_data['image_type'] . ';base64,' . base64_encode(file_get_contents($upload_data['full_path']));
			//echo "$base64";

			$this->httprequest->setParameter('image_name', $upload_data['file_name']);
			$this->httprequest->setParameter('image_file', $base64);
			@unlink($upload_data['full_path']);
			$this->session->set_userdata('userpic', "http://developer.rudii14.xyz/v2/user_img/" . $profpic);
		}
		else{
			echo $this->upload->display_errors();
		}

		echo $this->httprequest->execute();
		// update information yang ada
		$this->session->set_userdata('email', $email);
		$this->session->set_userdata('fullname', $fullname);
		$result = json_decode($this->httprequest->execute(), true);
		if($result['success'] == false){
			echo $result['errMsg'];
		}
		redirect(base_url() . 'member/profilepage/setting');

	}

	public function user_action(){
		$username = $this->input->post('username');
		$userType = $this->input->post('editType');
		$blokir = $this->input->post('toggleBlokir');
		$alasan_blokir = $this->input->post('blokir_alasan');

		$blokir = ($blokir=='Ya' ? 'Y' : 'N');
		$userType = ($userType=='User' ? 'user' : 'admin');

		$this->httprequest->init('user/update/'.$username, 'POST');
		$this->httprequest->setParameter('userType', $userType);
		$this->httprequest->setParameter('blokir', $blokir);
		$this->httprequest->setParameter('blokir_alasan', $alasan_blokir);
		$this->httprequest->setParameter('userType', $userType);
		$str = $this->httprequest->execute();
		echo $str;
		$result = json_decode($str, true);

		//var_dump($result);
		redirect(base_url() . "member/profilepage/user-list");
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url());
	}
}
