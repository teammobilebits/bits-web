<?php defined('BASEPATH') OR exit('No direct access allowed')?>

<h2>My Question</h2> <hr>
<div class="row">
	<div class="row" style="margin-bottom:10px; margin-right: 1px;">
		<a class="btn btn-primary pull-right" data-toggle="modal" data-target="#modalNewQuestion"><i class="glyphicon glyphicon-plus-sign"></i> New question</a>
	</div>
	<div class="row">
		<div class="col-md-offset-1 col-md-10">
			<table id="tblQuestionList" class="table table-striped table-bordered" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>#</th>
						<th>Question</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$i = 1;
						foreach($userQuestion as $uq ){
							$isi_content = htmlentities(strip_tags($uq['content']));
							$isi = substr($isi_content,0,200); // ambil sebanyak 150 karakter
							$isi = substr($isi_content,0,strrpos($isi," ")); // potong per spasi kalimat
							$isi .= ' ...';

							$questionUrl = base_url() . "question/$uq[id]/$uq[seo]";

							echo "<tr>";
							echo "<td>$i</td>
								  <td>
								  	<p><b><a href='$questionUrl' target='_blank'>".$uq['title']."</a></b></p>
								  	<p>$isi</p>
								  </td>
								  <td>".$uq['view']." views<br>".$uq['answer']." answer</td>
								  <td>
								  		<span>
								  			<a id='editQuestion' data-toggle='modal' data-target='#modalNewQuestion' data-id='".$uq['id']."' data-seo='".$uq['seo']."'><i class='glyphicon glyphicon-edit edit' ></i></a>    
											<a data-toggle='modal' data-id='".$uq['id']."' id='deleteQuestion' data-target='#deleteConf'><i class='glyphicon glyphicon-trash remove'></i></a>
								  		</span>
								  </td>";
							echo "</tr>";

							$i++;
						}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<!-- Modal untuk konfirmasi -->
<div class="modal fade" id="deleteConf" role="dialog">
	<div class="vertical-alignment-helper">
		<div class="modal-dialog modal-sm vertical-align-center">
			<div class="modal-content">
				<div class="modal-body">
					<div class="input-group">
						<div id="questionID" style="display: none;"></div>
						<div class="col-xs-12 text-center" style="margin-bottom: 50px;"><h4>Are you sure want to delete this question ?</h4></div>
						<div class="col-xs-4 col-xs-offset-1">
							<div class="btn btn-success" id="btnYes"><b>YES</b></div>
						</div>
						<div class="col-xs-4 col-xs-offset-2">
							<div class="btn btn-danger" data-dismiss="modal"><b>NO</b></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>