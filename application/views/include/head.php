<?php defined('BASEPATH') OR exit('No direct access allowed')?>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1"></meta>
<!-- tar generate pake php
<meta name="description" content=""> -->
<title><?php echo $title; ?></title>
<link rel="icon" href="">

<!-- Bootstrap CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
<!-- Datatable CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/datatables.min.css">
<!-- Quill CSS -->
<link href="<?php echo base_url(); ?>assets/css/quill-snow.css" rel="stylesheet">
<!-- W3Schools CSS 
<link href="<?php echo base_url(); ?>assets/css/w3.css" rel="stylesheet">-->
<!-- Custom stylesheet-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">