<?php defined('BASEPATH') OR exit('No direct access allowed')?>

<h2>User list</h2> <hr>
<!--<div class="row" style="margin-bottom:10px; margin-right: 1px;">
	<a class="btn btn-primary pull-right" data-toggle="modal" data-target="#modalAddAdmin"><i class="glyphicon glyphicon-plus-sign"></i> Admin</a>
</div> -->
<div class="row">
	<div class="col-md-12">
		<table id="tblUserList" class="table table-striped table-bordered" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>#</th>
					<th>Username</th>
					<th>Email</th>
					<th>User level</th>
					<th>Status</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php

					$i = 1;
					foreach($userlist as $a){
						echo "<tr>
								<td>$i</td>
								<td>".$a['username']."</td>
								<td>".$a['email']."</td>
								<td><b>".($a['type']=='admin' ? 'ADMIN' : 'USER')."</b></td>
								<td>".($a['blokir']=='Y' ? "<span class='label label-danger'>BLOCK</span>" : "<span class='label label-success'>ACTIVE</span>")."</b></td>
								<td>
									<a
										data-toggle='modal'
										id='adminEditUser'
										data-username='".$a['username']."'
										data-email='".$a['email']."'
										data-usertype='".$a['type']."'
										data-blokir='".$a['blokir']."'
										data-userpic='".$a['img']."'
										data-alasanblokir='".$a['blokir_alasan']."'
										data-target='#modalAdminEditUser' class='btn btn-primary btn-sm'><i class='glyphicon glyphicon-wrench remove'></i> Edit</a>
								</td>
							</tr>";
						$i++;
					}
				?>
			</tbody>
		</table>
	</div>
</div>

<!-- Modal untuk nambah admin -->
<div class="modal fade" id="modalAdminEditUser" role="dialog">
	<div class="vertical-alignment-helper">
		<div class="modal-dialog vertical-align-center">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" id="modalClose"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h3 class="modal-title">Edit user</h3>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-4">
							<img src="" class="img-responsive" id="userpic">
						</div>
						<div class="col-md-8">
							<form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>member/user-action" enctype="multipart/form-data">
								<input type="hidden" name="username"  id="username">
								<input type="hidden" name="action" value="updateuser">
								<div class="form-group">
									<label for="inputUsername" class="control-label col-md-3">Username</label>
									<div class="col-md-8">
										<p class="form-control-static" id="editUsername">asdasd</p>
									</div>
								</div>

								<div class="form-group">
									<label for="inputEmail" class="control-label col-md-3">Email</label>
									<div class="col-md-8">
										<p class="form-control-static" id="editEmail">asdasd</p>
									</div>
								</div>

								<div class="form-group">
									<label for="inputLink" class="control-label col-md-3">User Type</label>
									<div class="col-md-8">
										<select class="form-control" id="editType" name="editType">
											<option value="Administrator">Administrator</option>
											<option value="User">User</option>
										</select>
									</div>
								</div>

								<div class="form-group">
									<label for="" class="control-label col-md-3">Blokir</label>
									<div class="col-md-8">
										<select class="form-control" id="toggleBlokir" name="toggleBlokir">
											<option>Ya</option>
											<option>Tidak</option>
										</select>
									</div>
								</div>

								<div class="form-group">
									<label for="blokir_alasan" class="control-label col-md-3">Alasan Blokir</label>
									<div class="col-md-8">
										<textarea class="form-control" id="blokir_alasan" name="blokir_alasan"></textarea>
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-12">
										<input type="submit" value="Submit" class="btn btn-lg btn-primary btn-block">
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>