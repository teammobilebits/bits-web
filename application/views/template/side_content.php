<?php defined('BASEPATH') OR exit('No direct access allowed')?>

<div class="col-sm-4 col-md-4 hidden-xs" style="margin-bottom: 20px;">

	<!-- <div class="col-md-12">
		<img src="img/iklan.png" class="img-responsive">
	</div> -->
	<?php if ($this->session->has_userdata('signed_in') && $this->session->userdata('signed_in')): ?>
		<div class="col-md-12 sidePadding">
			<button class="btn btn-primary col-md-12" data-toggle='modal' data-target='#modalNewQuestion'><i class="glyphicon glyphicon-plus-sign"></i> Ask a question</button>
		</div>
	<?php endif ?>

	<?php if (isset($related)): ?>
		<div class="col-md-12 sidePadding">
			<div class="list-group">
				<a class='list-group-item active'><h4 class="list-group-item-heading">Related Question</h4></a>
				<?php
					foreach($related as $q){
						$url = base_url() . "question/" . $q['id'] . "/" . $q['seo'];
						echo "<a href='$url' class='list-group-item'>".$q['title']."</a>";
					}
				?>
			</div>
		</div>
	<?php endif ?>

	<div class="col-md-12 sidePadding">
		<div class="list-group">
			<a class='list-group-item active'><h4 class="list-group-item-heading">Trending Question</h4></a>
			<?php
				foreach($trending_question as $q){
					$url = base_url() . "question/" . $q['id'] . "/" . $q['seo'];
					echo "<a href='$url' class='list-group-item'>".$q['title']."</a>";
				}
			?>
		</div>
	</div>


</div>