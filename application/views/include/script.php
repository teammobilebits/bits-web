<?php defined('BASEPATH') OR exit('No direct access allowed')?>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datatables.min.js"></script>

<!-- quill text editor -->
<script src="<?php echo base_url(); ?>assets/js/quill.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/script.js"></script>

<noscript>
	<style type="text/css">
		#noJS{
			display: none;
		}
	</style>
	<h1>PLEASE ENABLE JAVASCRIPT</h1>
</noscript>