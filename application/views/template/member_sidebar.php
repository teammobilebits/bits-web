<?php defined('BASEPATH') OR exit('No direct access allowed')?>

<div class="profile-sidebar">
	<!-- SIDEBAR USERPIC -->
	<div class="profile-userpic">
		<img src="<?php echo $userpic; ?>" class="img-responsive" alt="">
	</div>

	<!-- SIDEBAR USER TITLE -->
	<div class="profile-usertitle">
		<div class="profile-usertitle-name">
			<?php echo $fullname; ?>
		</div>
		<div class="profile-usertitle-job">
			<?php
				switch($userType ){
					case "admin":
						echo "Administrator";
						break;
					case "moderator":
						echo "Moderator";
						break;
					default:
						echo "User";
				}
			?>
		</div>
	</div>

	<!-- SIDEBAR BUTTONS 
	<div class="profile-userbuttons">
		<button type="button" class="btn btn-success btn-sm">Follow</button>
		<button type="button" class="btn btn-danger btn-sm">Message</button>
	</div> -->

	<!-- SIDEBAR MENU -->
	<div class="profile-usermenu">
		<div class="panel-group" id="accordion">
			<!-- panel bagian overview -->
			<div class="panel panel-default">
				<a data-toggle="collapse" data-parent="#accordion" href="#collapseOverview" id="customPanelHeader">
					<div class="panel-heading">
						<h4 class="panel-title">
							<span class="glyphicon glyphicon-home">
							</span>  Overview
						</h4>
					</div>
				</a>
				<div id="collapseOverview" class="panel-collapse collapse <?php if($sidemenu=='overview')  echo "in";?>">
					<div class="panel-body">
						<table class="table">
							<tr>
								<td>
									<a href="<?php echo base_url(); ?>member/profilepage/my-question"> <span class="glyphicon glyphicon-question-sign"></span>  My Question </a>
								</td>
							</tr>
							<!-- <tr>
								<td>
									<a href="profilpage/overview/answer"> <span class="glyphicon glyphicon-comment"></span>  My Answer </a>
								</td>
							</tr> -->
						</table>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<a href="<?php echo base_url(); ?>member/profilepage/setting" id="customPanelHeader">
					<div class="panel-heading">
						<h4 class="panel-title">
							<span class="glyphicon glyphicon-user">
							</span>  Account Setting
						</h4>
					</div>
				</a>
			</div>

			<?php if($userType =='admin'){ ?>
			<div class="panel panel-default">
				<a data-toggle="collapse" data-parent="#accordion" href="#collapseAdmin">
					<div class="panel-heading">
						<h4 class="panel-title">
							<span class="glyphicon glyphicon-king">
							</span>  Administrator
						</h4>
					</div>
				</a>
				<div id="collapseAdmin" class="panel-collapse collapse <?php if($sidemenu=='admin')  echo "in";?>">
					<div class="panel-body">
						<table class="table">
							<tr>
								<td>
									<a href="<?php echo base_url(); ?>member/profilepage/user-list"> <span class="glyphicon glyphicon-user"></span>  User list</a>
								</td>
							</tr>
							<!--<tr>
								<td>
									<a href="profilpage/admin/adv"> <span class="glyphicon glyphicon-list-alt"></span>  Question list</a>
								</td>
							</tr>
							<tr>
								<td>
									<a href="<?php echo base_url(); ?>member/profilepage/announcement"> <span class="glyphicon glyphicon-bullhorn"></span>  Announcement</a>
								</td>
							</tr>-->
							<tr>
								<td><a href="<?php echo base_url(); ?>member/profilepage/report-list"> <span class="glyphicon glyphicon-leaf"></span>  Report List</a></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<?php } ?>
			
		</div>

		<!--<ul class="nav">
			<li class="<?php if($submenu=='overview') echo 'active'; ?>">
				<a href="profilpage/overview"> <i class="glyphicon glyphicon-home"></i> Overview </a>
			</li>
			<?php if($userType =='admin'){ ?>
			<li class="<?php if($submenu=='admin') echo 'active'; ?>">
				<a href="profilpage/admin"> <i class="glyphicon glyphicon-king"></i> Administrator </a>
			</li>
			<?php } ?>
			<li class="<?php if($submenu=='setting') echo 'active'; ?>">
				<a href="profilpage/setting"> <i class="glyphicon glyphicon-user"></i> Account Setting </a>
			</li>
		</ul> -->
	</div>
	<!-- END MENU -->
</div>