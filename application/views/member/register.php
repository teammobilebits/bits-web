<!DOCTYPE html>
<html lang="en">
<head>
	<?php echo $head; ?>
</head>


<body>
	<div id="noJS">
		<div id="customWrap">
			<?php echo $navbar; ?>

			<div class="container mainContainer">
				<div class="col-sm-12 col-md-12 formContainer">
					<div class="col-sm-12 col-md-offset-3 col-md-6">
						<?php if($msg != null){ ?>
						<div class="alert alert-dismissable fade in" style="background-color: red;">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
								<?php echo $msg; ?>
						</div>
						<?php } ?>

						<h2>UMN Stackoverflow register</h2>
						<form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>member/do-register">
							
							<div class="form-group">
								<label for="inputEmail" class="sr-only">Email Address</label>
								<input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" required autofocus>
							</div>

							<div class="form-group">
								<label for="inputFullName" class="sr-only">Full Name</label>
								<input type="text" id="inputFullName" name="fullname" class="form-control" placeholder="Full name" required autofocus>
							</div>

							<div class="form-group">
								<label for="inputUsername" class="sr-only">Username</label>
								<input type="text" id="inputUsername" name="username" class="form-control" placeholder="Username" required autofocus>
							</div>

							<div class="form-group">
								<label for="inputPwd" class="sr-only">Password</label>
								<input type="password" id="inputPwd" name="password" class="form-control" placeholder="Password" required autofocus>
							</div>

							<div class="form-group">
								<label for="inputConfPwd" class="sr-only">Confirm Password</label>
								<input type="password" id="inputConfPwd" name="confpassword" class="form-control" placeholder="Confirm password" required autofocus>
							</div>

							<div class="form-group">
								<input type="submit" value="Submit" class="btn btn-lg btn-primary btn-block">
							</div>
						</form>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	

	<?php echo $footer;?>
	<?php echo $script; ?>
</body>
</html>