<?php defined('BASEPATH') OR exit('No direct access allowed')?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php echo $head; ?>
</head>


<body>
	<div id="noJS">
		<div id="customWrap">
			<?php echo $navbar; ?>

			<div class="container mainContainer">
				<div class="col-sm-8 col-md-8">
					<h3>Recent question</h3> <hr>
					<?php
						

						$listQuestion = $recent;
						foreach($listQuestion as $q){
					?>
							<div class="row">
								<div class="col-xs-6 col-sm-1 col-md-1" style="margin-bottom: 20px;"><center><?php echo $q['view']; ?><br><small>view</small></center></div>
								<div class="col-xs-6 col-sm-1 col-md-1"><center><?php echo $q['total_answer']?><br><small>answer</small></center></div>
								<div class="col-xs-12 col-sm-10 col-md-10">
									<div class="col-md-12">
										<p><a href="<?php echo "question/".$q['id']."/".$q['seo'] ?>"><?php echo $q['title']?></a></p>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-8">
										<p><?php generateTags($q['tags']); ?></p>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-4">
										<p class='pull-right' style="color: grey; font-size: 12px;"><i><?php echo $q['action']; ?></i></p>
									</div>
									
								</div>
							</div> <hr>

					<?php } ?>
				</div>

				<?php echo $side_content; ?>
				
			</div>

			<?php echo $footer;?>
		</div>
	</div>
	

	
	<?php echo $script; ?>
</body>
</html>