<?php
	/**
	* 
	*/
	class HttpRequest{
		//var $base_url = 'http://developer.rudii14.xyz/v2/';
		var $base_url = 'http://localhost/bitsAPI_2/';
		var $url = '';
		var $param = array();
		var $method = '';
		
		public function init($url, $_method='GET')
		{
			$this->method = $_method;
			$this->url = $this->base_url . $url;
		}

		public function setParameter($key, $value){
			$this->param[$key] = $value;
		}

		private function setGetParam(){
			$var_ret = '';
			$i = 0;
			foreach ($this->param as $key => $value){
				if($i==0) $var_ret .= '?';
				else $var_ret .= '&';
				$var_ret .= ($key . '=' . urlencode($value));
				$i++;
			}
			return $var_ret;
		}

		private function resetRequest(){
			$this->param = array();
		}

		public function execute(){
			$result = '';

			if($this->method == 'POST'){
				$options = array(
					'http' => array(
						'header' => "Content-type: application/x-www-form-urlencoded\r\n",
						'method' => 'POST',
						'content' => http_build_query($this->param),

					)
				);
				$context = stream_context_create($options);
				$result = file_get_contents($this->url, true, $context);
			}
			else if($this->method == 'GET'){
				$result = file_get_contents($this->url . $this->setGetParam(), true);
			}

			$this->resetRequest();
			return $result;
		}
	}
?>