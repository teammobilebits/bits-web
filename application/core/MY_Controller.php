<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

	class CI_Custom_Controller extends CI_Controller {

		protected function configPage(){
			if($this->session->has_userdata('title')){
				$data['title'] = $this->session->userdata('title');
				$this->session->unset_userdata('title');	
			}
			else $data['title'] = "UMN StackOverflow";
			$navbar['navbar'] = $this->load->view('template/new-question-modal', NULL, TRUE);

			$data['head'] = $this->load->view('include/head', $data, TRUE);
			$data['script'] = $this->load->view('include/script', NULL, TRUE);
			$data['navbar'] = $this->load->view('template/navbar', $navbar, TRUE);
			$data['footer'] = $this->load->view('template/footer', NULL, TRUE);

			return $data;
		}

		protected function directAccessControl($isGranted, $url=''){
			if(!$isGranted) redirect(base_url() . $url);
		}
	}
?>