<?php
	require_once (APPPATH.'/libraries/Paging/Paging.php');

	class PagingPertanyaanByTag extends Paging{
		private $tagname;

		public function __construct($params){
			$batas = $params['batas'];
			$this->tagname = $params['tagname'];
			parent::__construct($batas);
		}

		public function getLink($i){
			//echo "no halaman $i";
			return base_url() . "question/by-tag?tag=".$this->tagname . "&page=" . $i;
		}
	}
?>